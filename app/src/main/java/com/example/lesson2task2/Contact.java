package com.example.lesson2task2;

public class Contact {

    private String mName;
    private int mAge;

    public void Contact(String name, int age){
        this.mName = name;
        this.mAge = age;
    }

    public String getName(){
        return mName;
    }

    public void setName(String name){
        mName = name;
    }

    public int getAge(){
        return mAge;
    }

    public void setAge(int age){
        mAge = age;
    }


}
