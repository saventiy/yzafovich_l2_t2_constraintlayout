package com.example.lesson2task2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private BottomNavigationView mBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager
                                      (getApplicationContext()));


        getSupportActionBar().setHomeAsUpIndicator(R.drawable.hamburger);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mBottomNavigationView = findViewById(R.id.bottom_navigation);
//        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mBottomNavigationView.getLayoutParams();
//        layoutParams.setBehavior(new BottomNavigationViewBehavior());


        updateUI();
    }

    private void updateUI(){
        ArrayList<Contact> contacts = new ArrayList<>();

        for(int i = 0; i != 20; ++i){
            Contact contact = new Contact();
            contact.setName("Person " + i);
            contact.setAge(i);
            contacts.add(contact);

        }

        Adapter adapter = new Adapter(contacts);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }


    private class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView mIconImageView;
        public TextView mNameTextView;
        public TextView mAgeTextView;

        public ViewHolder(View itemView){
            super(itemView);

            this.mIconImageView = itemView.findViewById(R.id.iconImageView);
            this.mNameTextView = itemView.findViewById(R.id.nameTextView);
            this.mAgeTextView = itemView.findViewById(R.id.ageTextView);


        }
    }


    private class Adapter extends RecyclerView.Adapter<ViewHolder>{

        private ArrayList<Contact> mContacts;

        public Adapter(ArrayList<Contact> contacts){
            this.mContacts = contacts;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);

            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Contact contact = mContacts.get(position);

            holder.mNameTextView.setText(String.valueOf(contact.getName()));
            holder.mAgeTextView.setText(String.valueOf(contact.getAge()));
        }

        @Override
        public int getItemCount() {
            return mContacts.size();
        }
    }




    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.tollbar_menu, menu);
        return true;
    }

}
